<?php


namespace App\DataTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\Client;

class PhoneInputDataTransformer implements DataTransformerInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritDoc
     */
    public function transform($object, string $to, array $context = [])
    {
        $this->validator->validate($object);

        /**
         * @var Client $existingClient
         */
        $existingClient = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];

        switch ($context['item_operation_name']) {
            case 'addPhone':
                $existingClient->addPhone($object->phone);
                break;

            case 'removePhone':
                $existingClient->removePhone($object->phone);
                break;
        }

        return $existingClient;
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Client) {
            return false;
        }

        return Client::class === $to && null !== ($context['input']['class'] ?? null);
    }
}