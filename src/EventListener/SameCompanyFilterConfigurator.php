<?php


namespace App\EventListener;


use App\Entity\User;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class SameCompanyFilterConfigurator
{
    private EntityManagerInterface $em;
    private Security $security;
    private Reader $reader;

    public function __construct(EntityManagerInterface $em, Security $security, Reader $reader)
    {
        $this->em = $em;
        $this->security = $security;
        $this->reader = $reader;
    }

    public function onKernelRequest(): void
    {
        if (($user = $this->getUser()) && !$this->security->isGranted("ROLE_ADMIN_BILEMO")) {
            $filter = $this->em->getFilters()->enable('same_company_filter');
            $filter->setParameter('companyId', $user->getId());
            $filter->setAnnotationReader($this->reader);
        }
    }

    private function getUser(): ?User
    {
        if (!$user = $this->security->getUser()) {
            return null;
        }

        return $user instanceof User ? $user : null;
    }

}