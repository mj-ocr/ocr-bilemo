<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Annotation\FilterOnCompany;
use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Dto\AddRemovePhoneInput;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"clients_write"}},
 *     normalizationContext={"groups"={"clients_read"}},
 *     attributes={"security"="is_granted('ROLE_ADMIN_COMPANY') and is_granted('IS_SAME_COMPANY', object)"},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN_COMPANY')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put",
 *         "delete",
 *         "addPhone"={
 *             "method"="PUT",
 *             "input"=AddRemovePhoneInput::class,
 *             "path"="/clients/{id}/addPhone",
 *             "requirements"={"id"="\d+"}
 *          },
 *          "removePhone"={
 *             "method"="PUT",
 *             "input"=AddRemovePhoneInput::class,
 *             "path"="/clients/{id}/removePhone",
 *             "requirements"={"id"="\d+"}
 *          },
 *     }
 * )
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 *
 * @FilterOnCompany("company_id")
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("clients_read")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=63)
     * @Groups({"clients_write", "clients_read"})
     */
    private $firstname;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=63)
     * @Groups({"clients_write", "clients_read"})
     */
    private $lastname;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="clients")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("clients_read")
     */
    private $company;

    /**
     * @ORM\ManyToMany(targetEntity=Phone::class)
     * @Groups({"clients_read"})
     */
    private $phones;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"clients_write", "clients_read"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     * @Groups({"clients_write", "clients_read"})
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=63, nullable=true)
     * @Groups({"clients_write", "clients_read"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=63, nullable=true)
     * @Groups({"clients_write", "clients_read"})
     */
    private $country;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getCompany(): ?User
    {
        return $this->company;
    }

    public function setCompany(?User $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|Phone[]
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    public function addPhone(Phone $phone): self
    {
        if (!$this->phones->contains($phone)) {
            $this->phones[] = $phone;
        }

        return $this;
    }

    public function removePhone(Phone $phone): self
    {
        if ($this->phones->contains($phone)) {
            $this->phones->removeElement($phone);
        }

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }
}
