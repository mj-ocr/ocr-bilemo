<?php


namespace App\Dto;


use App\Entity\Phone;
use Symfony\Component\Serializer\Annotation\Groups;

final class AddRemovePhoneInput
{
    /**
     * @var Phone $phone
     *
     * @Groups("clients_write")
     */
    public Phone $phone;
}